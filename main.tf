provider "aws" {
  region = "eu-central-1" # update this to your preferred AWS region
}

module "eks" {
  source  = "terraform-aws-modules/eks/aws"
  version = "17.1.0" # update this to the latest version

  cluster_name = "my-eks-cluster"
  cluster_version = "1.21"
  subnets = ["subnet-abcde012", "subnet-bcde012a", "subnet-fghi345a"]
  vpc_id = "vpc-073616180e20bafde"

  node_groups = {
    eks_nodes = {
      desired_capacity = 2
      max_capacity     = 10
      min_capacity     = 1

      instance_type = "t3.medium"
      key_name      = "my-key-name"
    }
  }
}

output "cluster_endpoint" {
  description = "Endpoint for EKS control plane."
  value       = module.eks.cluster_endpoint
}

output "cluster_security_group_id" {
  description = "Security group ID attached to the EKS cluster."
  value       = module.eks.cluster_security_group_id
}

output "cluster_iam_role_name" {
  description = "IAM role name associated with EKS cluster."
  value       = module.eks.cluster_iam_role_name
}

output "cluster_certificate_authority_data" {
  description = "Nested attribute containing certificate-authority-data for your cluster."
  value       = module.eks.cluster_certificate_authority_data
}
